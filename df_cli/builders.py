import pathlib
import inspect
import stat

from .models import Config


SHELL_HEADER = '#!/usr/bin/env zsh'


def normalize_aliases(aliases: dict):
    ret = dict()
    dupes = []

    for k, v in aliases.items():
        if isinstance(v, dict):
            key = v.get('alias', k)

            if key in ret:
                dupes.append(key)

            ret[key] = dict(
                description=v.get('description'),
                command=v.get('command')
            )

        else:
            key = v[1] or k

            if key in ret:
                dupes.append(key)

            ret[key] = dict(
            description=v[2],
            command=v[0]
        )

    return ret, dupes


def build_commands(config: Config, commands_dir: pathlib.Path):
    for name, opts in config.commands.items():
        f_path = commands_dir / f'{name}'

        with open(f_path, 'w') as f:
            f.writelines([
                SHELL_HEADER,
                '\n\n',
                inspect.cleandoc(opts.get('command', '')),
                '\n',
            ])

        f_path.chmod(f_path.stat().st_mode | stat.S_IXUSR)


def build_aliases(config: Config, aliases_path: pathlib.Path):
    aliases, dupes = normalize_aliases(config.aliases)

    if dupes:
        print(f'ERROR: Duplicate aliases found: {dupes}')
        exit(1)

    with open(aliases_path, 'w') as f:
        f.writelines([
            SHELL_HEADER,
            '\n\n',
            *(
                f'alias {k}=\'{v["command"]}\'\n'
                for k, v in aliases.items()
            ),
        ])

    aliases_path.chmod(aliases_path.stat().st_mode | stat.S_IXUSR)


def build_functions(config: Config, functions_path: pathlib.Path):
    with open(functions_path, 'w') as f:
        lines=  [
            SHELL_HEADER,
            '\n\n'
        ]

        for name, body in config.functions.items():
            lines.extend([
                f'\n{name}() {{\n',

                    inspect.cleandoc(body.get('command', ''))
                if body.get('skip_indent', None) else
                    ''.join(f'\t{s}\n' for s in inspect.cleandoc(body.get('command', '')).split('\n')),

                '}\n'
            ])

        f.writelines(lines)

    functions_path.chmod(functions_path.stat().st_mode | stat.S_IXUSR)
