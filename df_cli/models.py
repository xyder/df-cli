from dataclasses import dataclass


@dataclass
class Config:
    aliases: dict
    commands: dict
    functions: dict


def merge_configs(c1: Config, c2: Config):
    return Config(
        aliases={**c1.aliases, **c2.aliases},
        commands={**c1.commands, **c2.commands},
        functions={**c1.functions, **c2.functions}
    )
