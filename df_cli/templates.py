envrc = '''\
# to check what gets loaded here, visit: https://github.com/xyder/dotfiles/blob/master/.envrc
. ~/dotfiles/.envrc


# load env vars from file (preferred method):
# set -a
# source <(nxc -lf .env)
# set +a

# or:

# export $(nxc -lf .env | xargs -d '\\n')

# shortcut functions
# with source:

# xps nxc -lf .env
# xpns -lf .env

# with xargs:
# xpx nxc -lf .env
# xpnx -lf .env


# create commands in the dfc_config module. aliases and functions are not loaded automatically
# initialize a dfc_config with `dc init --dfc-file` or `dc init --dfc-dir`
'''

dfc_config_file = '''
commands = dict(
    app=dict(
        description='Run the application',
        command="""
            <enter the command here>
        """
    ),
)

aliases = dict(
    # alias_name=dict(
    #     alias='alias_alias',
    #     description='Enter a description',
    #     command='echo $@'
    # ),
    # or ('command', 'alias', 'description')
)

functions = dict(
    # function_name=dict(
    #     description='The function description',
    #     command="""
    #         <enter the command here>
    #     """
    # )
)
'''

dfc_config_module_init = '''\
from . import commands, functions, aliases


commands = commands.commands
functions = functions.functions
aliases = aliases.aliases
'''

dfc_config_module_aliases = '''
aliases = dict(
    # alias_name=dict(
    #     alias='alias_alias',
    #     description='Enter a description',
    #     command='echo $@'
    # ),
    # or ('command', 'alias', 'description')
)
'''

dfc_config_module_commands = '''
commands = dict(
    app=dict(
        description='Run the application',
        command="""
            <enter the command here>
        """
    ),
)
'''

dfc_config_module_functions = '''
functions = dict(
    # function_name=dict(
    #     description='The function description',
    #     command="""
    #         <enter the command here>
    #     """
    # )
)
'''
