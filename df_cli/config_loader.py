import pathlib
import importlib
import importlib.util
import sys
import yaml

from .models import Config


def _process_path(path: pathlib.Path = None):
    if path:
        if not path.exists():
            return (None, False, False)

        return (path, path.is_file(), path.suffix in ['.yaml', '.yml'])

    path_yaml = pathlib.Path('dfc_config.yaml')
    path_file = pathlib.Path('dfc_config.py')
    path_dir = pathlib.Path('dfc_config')

    if path_yaml.is_file():
        return (path_yaml, True, True)

    if path_file.is_file():
        return (path_file, True, False)

    if path_dir.is_dir():
        return (path_dir, False, False)

    return (None, False, False)


def _module_from_file(path: pathlib.Path):
    module_name = '.'.join(path.name.split('.')[0:-1])

    spec = importlib.util.spec_from_file_location(module_name, path.absolute())
    config = importlib.util.module_from_spec(spec)

    spec.loader.exec_module(config)

    return config

def _module_from_dir(path: pathlib.Path):
    module_name = path.name

    sys.path.insert(0, str(path.parent.absolute()))

    return importlib.import_module(module_name)


def _load_from_py(path: pathlib.Path = None, is_file: bool = True):
    try:
        config = _module_from_file(path) if is_file else _module_from_dir(path)
    except (FileNotFoundError, AttributeError) as e:
        return (False, f'ERROR: "{path.absolute()}" is not a valid python module. Error: {e}')

    return (True, Config(
        commands=getattr(config, 'commands', dict()),
        aliases=getattr(config, 'aliases', dict()),
        functions=getattr(config, 'functions', dict()),
    ))


def _load_from_yaml(path: pathlib.Path = None):
    config = dict()

    with open(path, 'r') as f:
        try:
            config = yaml.load(f, Loader=yaml.FullLoader)
        except yaml.YAMLError as e:
            return (False, f'ERROR: config file is invalid: {e}')

    config = config or dict()
    config = {
        **dict(
            variables=dict(),
            aliases=dict(),
            functions=dict(),
            commands=dict()
        ),
        **config
    }

    def interpolate(chunk, variables):
        chunk = chunk or dict()

        for k, v in chunk.items():
            chunk[k] = {
                **(chunk[k] or dict()),
                'command': ((chunk[k] or dict()).get('command', '')).format_map(variables)
            }

        return chunk


    variables = config['variables'] or dict()
    config['commands'] = interpolate(config['commands'], variables)
    config['aliases'] = interpolate(config['aliases'], variables)

    # todo: temporary replacement for the dynamic up movers
    config['aliases'] = {
        **config['aliases'],
        **{
            f'cd{i}': (f'cd {"/".join(i * [".."])}', (i + 1) * '.', f'Go up {i} level(s)')
            for i in range(1, 6)
        }
    }

    config['functions'] = interpolate(config['functions'], variables)

    return (True, Config(
        commands=config['commands'],
        aliases=config['aliases'],
        functions=config['functions'],
    ))


def load_config(config_path: pathlib.Path = None, use_yaml = False):
    path, is_file, is_yaml = _process_path(config_path)

    if not path:
        return (False, f'Config {config_path} is not valid or does not exist.')

    if is_yaml:
        return _load_from_yaml(path=path)
    else:
        return _load_from_py(path=path, is_file=is_file)
