
import pathlib

from plumbum import cli, colors

from .models import Config, merge_configs
from .builders import build_aliases, build_commands, build_functions, normalize_aliases
from .config_loader import load_config


commands_dir = pathlib.Path('.direnv/commands')
aliases_path = pathlib.Path('.direnv/aliases_config')
functions_path = pathlib.Path('.direnv/functions_config')

# todo: add conditions, such as: shutil.which('exa')
# todo: add augmenters, such as up movers custom code
# todo: maybe switch to using an automatic class converter (using munch) and allowing for interpolating any value in the yaml
# example: command: {commands.dc.command} build
# todo: interpolate environment variables
# todo: variables cross-interpolation
# todo: generate a yaml config
# todo: generate commands automatically from npm and other
# todo: check for changes in config file

class DFCli(cli.Application):
    config_path = cli.SwitchAttr('--config-path', str, help='Path to a custom config module')

    with_global = cli.Flag('-g', help='Include global config into listing options. Only applies to `list`')

    def main(self, *args):
        if args:
            print(f'Unknown command {args[0]}')
            return 1

        if not self.nested_command:
            print('No command given.')
            return 1

        # skip for commands that don't need a config
        if self.nested_command[0] == InitConfig:
            return

        global config
        is_ok, config = load_config(pathlib.Path(self.config_path) if self.config_path else None)

        if self.with_global:
            is_ok_global, config_global = load_config(pathlib.Path('~/dotfiles/dfc_config.yaml').expanduser())

            if is_ok_global:
                if is_ok:
                    config = merge_configs(config_global, config)
                else:
                    config = config_global
            else:
                if not is_ok:
                    print(config)
                    exit(1)
        else:
            if not is_ok:
                print(config)
                exit(1)


@DFCli.subcommand('build')
class BuildCommands(cli.Application):
    """ Build the commands and aliases """

    @staticmethod
    def _setup_commands(config: Config, commands_dir: pathlib.Path):
        commands_dir.mkdir(parents=True, exist_ok=True)

        # cleanup missing
        for f_path in commands_dir.iterdir():
            if not f_path.name in config.commands.keys():
                f_path.unlink(missing_ok=True)

    @classmethod
    def main(cls):
        cls._setup_commands(config, commands_dir)

        aliases, dupes = normalize_aliases(config.aliases)

        keys = list(config.commands.keys())
        keys.extend(aliases.keys())
        keys.extend(config.functions.keys())

        acc = []
        for key in keys:
            if key in acc:
                dupes.append(key)
                continue

            acc.append(key)

        if dupes:
            print(f'ERROR: duplicate keys found: {dupes}')
            exit(1)

        dest_dir =  commands_dir.parent.absolute()
        report = dict()

        if config.commands:
            report['commands'] = len(config.commands)
            build_commands(config, commands_dir)

        if config.aliases:
            report['aliases'] = len(config.aliases)
            build_aliases(config, aliases_path)

        if config.functions:
            report['functions'] = len(config.functions)
            build_functions(config, functions_path)

        print(f'[df-cli] finished building: {", ".join(f"{v} {k}" for k, v in report.items())} in {dest_dir} ..')


@DFCli.subcommand('list')
class ListCommands(cli.Application):
    """ List the commands and aliases """

    def main(self):
        """ meant to be used with fzf """

        commands = [dict(
            name=k,
            description=v.get('description', 'No description available'),
            command = commands_dir.absolute() / k
        ) for k, v in config.commands.items()]

        commands.extend(
            dict(
                name=v.get('alias', k),
                description=v.get('description', 'No description available'),
                command=v.get('command', 'No command associated')
            ) if isinstance(v, dict) else
            dict(
                name=v[1] or k,
                description=v[2] or 'No description available',
                command=v[0] or 'No command associated',
            ) for k, v in config.aliases.items()
        )

        commands.extend(
            dict(
                name=k,
                description=v.get('description', 'No description available'),
                command=k
            ) for k, v in config.functions.items()
        )

        for command_body in commands:
            name, description, command = (command_body[k] for k in ('name', 'description', 'command'))
            print(f'{colors.green | name}\0\t{colors.cyan | description}\t\0{command}')


@DFCli.subcommand('init')
class InitConfig(cli.Application):
    """ init files """

    envrc = cli.Flag('--envrc', help='init an .envrc file if it doesn\'t exist')
    dfc_file_module = cli.Flag('--dfc-file', help='init a dfc_config.py file module')
    dfc_dir_module = cli.Flag('--dfc-dir', help='init a dfc_config module')

    def main(self):
        from . import templates

        if self.dfc_dir_module and self.dfc_file_module:
            print('Both --dfc-file and --dfc-dir cannot be specified at the same time.')
            exit(1)

        if self.envrc:
            if pathlib.Path('.envrc').exists():
                print('.envrc already exists.')
                exit(1)

            with open('.envrc', 'w') as f:
                f.write(templates.envrc)

        if self.dfc_file_module:
            if pathlib.Path('dfc_config.py').exists() or pathlib.Path('dfc_config').exists():
                print('dfc_config already exists.')
                exit(1)

            with open('dfc_config.py', 'w') as f:
                f.write(templates.dfc_config_file)

        if self.dfc_dir_module:
            if pathlib.Path('dfc_config.py').exists() or pathlib.Path('dfc_config').exists():
                print('dfc_config already exists.')
                exit(1)

            dfc_dir = pathlib.Path('dfc_config')
            dfc_dir.mkdir(parents=True, exist_ok=True)

            with open(dfc_dir / '__ini__.py', 'w') as f:
                f.write(templates.dfc_config_module_init)

            with open(dfc_dir / 'aliases.py', 'w') as f:
                f.write(templates.dfc_config_module_aliases)

            with open(dfc_dir / 'commands.py', 'w') as f:
                f.write(templates.dfc_config_module_commands)

            with open(dfc_dir / 'functions.py', 'w') as f:
                f.write(templates.dfc_config_module_functions)

@DFCli.subcommand('print')
class PrintCommands(cli.Application):
    """ Prints commands as shell code """

    def main(self):
        """ meant to be used with fzf """

        aliases = [dict(
            name=v.get('alias', k),
            description=v.get('description', 'No description available'),
            command=v.get('command', 'No command associated')
        ) if isinstance(v, dict) else
        dict(
            name=v[1] or k,
            description=v[2] or 'No description available',
            command=v[0] or 'No command associated',
        ) for k, v in config.aliases.items()]

        commands = [dict(
            name=k,
            description=v.get('description', 'No description available'),
            command=v.get('command', 'No command available')
        ) for k, v in config.commands.items()]

        commands.extend(
            dict(
                name=k,
                description=v.get('description', 'No description available'),
                command=v.get('command', 'No command available')
            ) for k, v in config.functions.items()
        )

        for alias in aliases:
            name, description, command = (alias[k] for k in ('name', 'description', 'command'))
            description = '# ' + description + '\n' if description else ''
            print(f"{description}alias {name}='{command}'")

        for command_body in commands:
            name, description, command = (command_body[k] for k in ('name', 'description', 'command'))
            print(f'# {description}\n{name}()' + '{\n' + str(command) + '\n}\n')

def main():
    DFCli.run()


if __name__ == '__main__':
    main()
