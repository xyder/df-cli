commands = dict(
    test_dfc=dict(
        description='A typical test command that could be run',
        command="""
            print $@
            echo $@
        """
    )
)

aliases = dict(
    alias1=dict(
        alias='alias1',
        description='A typical alias',
        command='echo $@'
    )
)

functions = dict(
    function1=dict(
        description='the description of the function',
        command='''
            echo $@
        '''
    )
)
